<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Modules\Auth\Http\Dto\AuthorizeDto;
use Modules\Auth\Http\Dto\RegisterDto;
use Modules\Auth\Http\Requests\AuthorizeRequest;
use Modules\Auth\Http\Requests\RegisterRequest;
use Modules\Auth\Http\Services\Authorizer;
use Modules\Auth\Http\Services\Registrar;

/**
 * @group Аутенфикация
 */
class AuthController
{
    public function __construct(
        private readonly Registrar  $registrator,
        private readonly Authorizer $authorizer,
    )
    {
    }

    /**
     * Регистрация
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $dto = $request->asDto(RegisterDto::class);
        $token = $this->registrator->register($dto);
        return response()->json(compact('token'));
    }

    /**
     * Авторизация
     */
    public function authorize(AuthorizeRequest $request): JsonResponse
    {
        $dto = $request->asDto(AuthorizeDto::class);
        $token = $this->authorizer->authorize($dto);
        return response()->json(compact('token'));
    }
}
