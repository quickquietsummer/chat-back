<?php

namespace Modules\Auth\Http\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Http\Dto\AuthorizeDto;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Authorizer
{
    public function __construct(private readonly TokenIssuer $tokenIssuer)
    {
    }

    public function authorize(AuthorizeDto $dto): string
    {
        $user = User::whereEmail($dto->email)->firstOrFail();

        $isPasswordsEquals = Hash::check($dto->password, $user->password);
        if (!$isPasswordsEquals) {
            throw new HttpException(403, 'Пароль не подходит');
        }

        return $this->tokenIssuer->refresh($user);
    }
}
