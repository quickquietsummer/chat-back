<?php

namespace Modules\Auth\Http\Services;

use App\Models\User;

class TokenIssuer
{
    public function create(User $user): string
    {
        $token = $user->createToken('auth');
        return $token->plainTextToken;
    }

    public function refresh(User $user): string
    {
        $user->tokens()->delete();
        return $this->create($user);
    }
}
