<?php

namespace Modules\Auth\Http\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Http\Dto\RegisterDto;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Registrar
{
    public function __construct(private readonly TokenIssuer $tokenIssuer)
    {
    }

    /**
     * Возвращает токен
     *
     * @param RegisterDto $dto
     * @return string
     * @throws HttpException
     */
    public function register(RegisterDto $dto): string
    {
        $emailExists = User::whereEmail($dto->email)->exists();
        if ($emailExists) {
            throw new HttpException(403, "Почта $dto->email уже существует");
        }
        $user = new User();
        $user->email = $dto->email;
        $user->name = $dto->name;
        $user->password = Hash::make($dto->password);
        $user->save();
        return $this->tokenIssuer->create($user);
    }
}
