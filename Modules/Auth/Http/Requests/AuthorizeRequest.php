<?php

namespace Modules\Auth\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

/**
 * @bodyParam email string Почта
 * @bodyParam password string Пароль
 */
class AuthorizeRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => ['email', Rule::exists('users', 'email')],
            'password' => ['string', 'required'],
        ];
    }
}
