<?php

namespace Modules\Auth\Http\Requests;

use App\Http\Requests\BaseRequest;

/**
 * @bodyParam name string Имя
 * @bodyParam email string Почта
 * @bodyParam password string Пароль
 * @bodyParam password_confirmation string Пароль подтверждение
 */
class RegisterRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['string', 'required', 'min:4', 'max:50'],
            'email' => ['email'],
            'password' => ['string', 'required', 'min:5', 'confirmed'],
        ];
    }
}
