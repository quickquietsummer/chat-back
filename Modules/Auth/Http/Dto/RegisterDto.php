<?php

namespace Modules\Auth\Http\Dto;

use App\Http\Dto\Dto;

class RegisterDto extends Dto
{
    public string $name;
    public string $password;
    public string $email;
}
