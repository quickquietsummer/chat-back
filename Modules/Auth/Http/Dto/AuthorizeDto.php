<?php

namespace Modules\Auth\Http\Dto;

use App\Http\Dto\Dto;

class AuthorizeDto extends Dto
{
    public string $email;
    public string $password;
}
