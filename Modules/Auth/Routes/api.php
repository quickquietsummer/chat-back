<?php

use Modules\Auth\Http\Controllers\AuthController;

Route::prefix('/auth')->group(function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/authorize', [AuthController::class, 'authorize']);
});
