<?php

namespace Modules\Message\Entities;

use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Modules\Chat\Entities\Chat;
use Modules\Message\Database\factories\MessageFactory;

/**
 * Modules\Message\Entities\Message
 *
 * @property int $id
 * @property int $sender_id
 * @property int $chat_id
 * @property string $text
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Message newModelQuery()
 * @method static Builder|Message newQuery()
 * @method static Builder|Message query()
 * @method static Builder|Message whereChatId($value)
 * @method static Builder|Message whereCreatedAt($value)
 * @method static Builder|Message whereId($value)
 * @method static Builder|Message whereSenderId($value)
 * @method static Builder|Message whereText($value)
 * @method static Builder|Message whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Chat|null $chat
 * @property-read User $user
 * @method static \Modules\Message\Database\factories\MessageFactory factory(...$parameters)
 */
class Message extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory(): MessageFactory
    {
        return MessageFactory::new();
    }

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }
}
