<?php

namespace Modules\Message\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Message\Entities\Message;

class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'text' => $this->faker->realText
        ];
    }
}

