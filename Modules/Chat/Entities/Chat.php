<?php

namespace Modules\Chat\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Modules\Chat\Database\factories\ChatFactory;
use Modules\Message\Entities\Message;

/**
 * Modules\Chat\Entities\Chat
 *
 * @method static ChatFactory factory(...$parameters)
 * @method static Builder|Chat newModelQuery()
 * @method static Builder|Chat newQuery()
 * @method static Builder|Chat query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $owner_id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Chat whereCreatedAt($value)
 * @method static Builder|Chat whereId($value)
 * @method static Builder|Chat whereName($value)
 * @method static Builder|Chat whereOwnerId($value)
 * @method static Builder|Chat whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Message[] $messages
 * @property-read int|null $messages_count
 * @property-read User $owner
 */
class Chat extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return ChatFactory::new();
    }

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class);
    }
}
