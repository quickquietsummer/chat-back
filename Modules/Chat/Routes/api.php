<?php

use Modules\Chat\Http\Controllers\ChatController;

Route::prefix('/chat')->group(function () {
    Route::middleware('auth:sanctum')->get('/create', [ChatController::class, 'create']);
});
