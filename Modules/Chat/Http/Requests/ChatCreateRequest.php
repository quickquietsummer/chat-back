<?php

namespace Modules\Chat\Http\Requests;

use App\Http\Requests\BaseRequest;

class ChatCreateRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name'=>['required','string','max:30']
        ];
    }
}
