<?php

namespace Modules\Chat\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Chat\Http\Dto\ChatCreateDto;
use Modules\Chat\Http\Requests\ChatCreateRequest;

class ChatController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request): JsonResponse
    {
        return response()->json([]);
    }

    /**
     * Создать чат
     */
    public function create(ChatCreateRequest $request): JsonResponse
    {
        $dto = $request->asDto(ChatCreateDto::class);
        return response()->json([]);
    }

    public function store(Request $request): JsonResponse
    {
        return response()->json([]);
    }

    public function show(Request $request): JsonResponse
    {
        return response()->json([]);
    }

    public function edit(Request $request): JsonResponse
    {
        return response()->json([]);
    }

    public function update(Request $request): JsonResponse
    {
        return response()->json([]);
    }

    public function destroy(Request $request): JsonResponse
    {
        return response()->json([]);
    }
}
