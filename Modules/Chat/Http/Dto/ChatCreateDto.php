<?php

namespace Modules\Chat\Http\Dto;

use App\Http\Dto\Dto;

class ChatCreateDto extends Dto
{
    public string $name;
}
