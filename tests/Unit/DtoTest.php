<?php

namespace Tests\Unit;

use InvalidArgumentException;
use Mockery\Mock;
use Modules\Auth\Http\Dto\RegisterDto;
use PHPUnit\Framework\TestCase;

class DtoTest extends TestCase
{
    public function test_that_dto_fill_properties()
    {
        $data = [
            'name' => 'ALAHAHAHA',
            'password' => '12345678',
            'email' => 'stewt@mail.ru'
        ];
        $registerDTO = new RegisterDto($data);

        $this->assertEquals($data['name'], $registerDTO->name);
        $this->assertEquals($data['password'], $registerDTO->password);
        $this->assertEquals($data['email'], $registerDTO->email);
    }
    public function test_that_dto_throw_exception_if_not_all_data()
    {
        $this->expectException(InvalidArgumentException::class);
        $data = [
            'name' => 'ALAHAHAHA',
            'email' => 'stewt@mail.ru'
        ];
        new RegisterDto($data);
    }
}
