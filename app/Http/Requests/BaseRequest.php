<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    public function asDto(string $dtoClass)
    {
        return new $dtoClass($this->validated());
    }
}
