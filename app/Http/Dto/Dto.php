<?php

namespace App\Http\Dto;

use InvalidArgumentException;

class Dto
{
    public function __construct(array $data)
    {
        $properties = collect(get_class_vars(static::class));
        $properties = $properties->merge(get_object_vars($this))->keys();

        foreach ($properties as $property) {
            if (empty($data[$property])) {
                throw new InvalidArgumentException("Свойство ДТО $property отсутствует");
            }
            $this->{$property} = $data[$property];
        }
    }
}
